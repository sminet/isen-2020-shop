package fr.yncrea.cir3.shop.service;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CartProductDto implements Serializable {
    private Long productId;
    private Long quantity;
}
