package fr.yncrea.cir3.shop.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@SessionScope
public class CartService implements Serializable {
    private List<CartProductDto> products = new ArrayList<>();

    public void addProduct(Long productId, Long quantity) {
        // TODO Si le produit existe déjà, incrémenter son nombre
        // création d'une ligne du panier
        CartProductDto product = new CartProductDto();
        product.setProductId(productId);
        product.setQuantity(quantity);

        products.add(product);
    }

    public List<CartProductDto> getProducts() {
        // On retourne une collection non modifiable
        // permet d'eviter que le contrôleur puisse ajouter produit
        // directement dans la liste (si on retourne une référence vers elle)
        return Collections.unmodifiableList(products);
    }
}
