package fr.yncrea.cir3.shop.service;

import fr.yncrea.cir3.shop.domain.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Slf4j
@Service
public class ProductPictureService {
    @Value("${application.upload-path:var/file-upload}")
    private String uploadPath;

    public void save(Product product, MultipartFile picture) {
        String fileName = StringUtils.cleanPath(picture.getOriginalFilename());

        try {
            Path path = Paths.get(uploadPath, product.getId().toString());
            Files.copy(picture.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Impossible de sauvegarder l'image du produit", e);
            return;
        }

        product.setPictureOriginalFilename(fileName);
    }

    public Resource loadAsResource(Product product) {
        Path path = Paths.get(uploadPath, product.getId().toString());
        try {
            return new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            log.error("Impossible d'accéder à l'image du produit", e);
            return null;
        }
    }

    public void delete(Product product) {
        Path path = Paths.get(uploadPath, product.getId().toString());
        try {
            Files.delete(path);
        } catch (IOException e) {
            log.error("Impossible de supprimer l'image du produit", e);
        }

        product.setPictureOriginalFilename(null);
    }
}
