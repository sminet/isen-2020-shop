package fr.yncrea.cir3.shop.repository;

import fr.yncrea.cir3.shop.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
}
