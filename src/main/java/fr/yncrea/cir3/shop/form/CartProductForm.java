package fr.yncrea.cir3.shop.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CartProductForm {
    @NotNull
    private Long productId;

    @Min(1)
    private Long quantity;
}
