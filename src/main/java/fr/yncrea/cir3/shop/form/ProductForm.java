package fr.yncrea.cir3.shop.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class ProductForm {
    private Long id;

    @NotBlank
    @Size(min = 5, max = 255)
    private String name;

    private String description;

    @NotNull
    @Min(0)
    private Double price;

    private Long category;
    private Set<Long> tags;

    private String pictureOriginalFilename;
}
