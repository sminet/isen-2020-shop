package fr.yncrea.cir3.shop.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CategoryForm {
    private Long id;

    @NotBlank
    @Size(min = 2, max = 255)
    private String name;
}
