package fr.yncrea.cir3.shop.controller;

import fr.yncrea.cir3.shop.domain.Category;
import fr.yncrea.cir3.shop.form.CategoryForm;
import fr.yncrea.cir3.shop.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/category")
@Secured("ROLE_ADMIN")
public class CategoryController {
    @Autowired
    private CategoryRepository categories;

    @GetMapping({"/", "/list"})
    public String list(Model model) {
        model.addAttribute("categories", categories.findAll());
        return "category/list";
    }

    @GetMapping({"/add", "edit/{id}"})
    public String add(@PathVariable(required = false) Long id, Model model) {
        CategoryForm form = new CategoryForm();
        model.addAttribute("category", form);

        if (id != null) {
            Category c = categories.findById(id).orElseThrow(() -> new RuntimeException("Not found"));

            form.setId(c.getId());
            form.setName(c.getName());
        }

        return "category/add";
    }

    @PostMapping("/add")
    public String addAction(@Valid @ModelAttribute("category") CategoryForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("category", form);
            return "category/add";
        }

        Category c = new Category();

        if (form.getId() != null) {
            c = categories.findById(form.getId()).orElseThrow(() -> new RuntimeException("Not found"));
        }

        c.setName(form.getName());
        categories.save(c);

        return "redirect:/category/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        categories.deleteById(id);
        return "redirect:/category/list";
    }
}
