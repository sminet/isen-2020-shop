package fr.yncrea.cir3.shop.controller;

import fr.yncrea.cir3.shop.domain.Category;
import fr.yncrea.cir3.shop.domain.Product;
import fr.yncrea.cir3.shop.domain.Tag;
import fr.yncrea.cir3.shop.form.ProductForm;
import fr.yncrea.cir3.shop.repository.CategoryRepository;
import fr.yncrea.cir3.shop.repository.ProductRepository;
import fr.yncrea.cir3.shop.repository.TagRepository;
import fr.yncrea.cir3.shop.service.ProductPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

@Controller
@RequestMapping("/product")
@Secured("ROLE_ADMIN")
public class ProductController {
    @Autowired
    private ProductRepository products;

    @Autowired
    private CategoryRepository categories;

    @Autowired
    private TagRepository tags;

    @Autowired
    private ProductPictureService pictureService;

    @GetMapping({"", "/", "/list"})
    public String list(Model model) {
        model.addAttribute("products", products.findAll());
        return "product/list";
    }

    @GetMapping({"/add", "edit/{id}"})
    public String add(@PathVariable(required = false) Long id, Model model) {
        ProductForm form = new ProductForm();
        model.addAttribute("product", form);
        model.addAttribute("categories", categories.findAll());
        model.addAttribute("tags", tags.findAll());

        if (id != null) {
            Product p = products.findById(id).orElseThrow(() -> new RuntimeException("Not found"));

            // copie les données simples
            form.setId(p.getId());
            form.setName(p.getName());
            form.setDescription(p.getDescription());
            form.setPrice(p.getPrice().doubleValue() / 100); // converti de cents en euro
            form.setPictureOriginalFilename(p.getPictureOriginalFilename());

            // copie la catégorie
            if (p.getCategory() != null) {
                form.setCategory(p.getCategory().getId());
            }

            // copie les tags
            Set<Long> tSet = new LinkedHashSet<>();
            for (Tag t: p.getTags()) {
                tSet.add(t.getId());
            }
            form.setTags(tSet);

            // peut aussi d'écrire en une ligne
            // form.setTags(p.getTags().stream().map(Tag::getId).collect(Collectors.toSet()));
        }

        return "product/add";
    }

    @PostMapping("/add")
    public String addAction(
            @Valid @ModelAttribute("product") ProductForm form, BindingResult result, Model model,
            @RequestParam(value = "picture", required = false) MultipartFile picture
    ) {
        if (result.hasErrors()) {
            model.addAttribute("product", form);
            model.addAttribute("categories", categories.findAll());
            model.addAttribute("tags", tags.findAll());

            return "product/add";
        }

        // si on est en création, création d'un nouvel objet
        Product p = new Product();

        // si on est en édition, charge l'objet depuis la base
        if (form.getId() != null) {
            p = products.findById(form.getId()).orElseThrow(() -> new RuntimeException("Not found"));
        }

        // copie des champs simples
        p.setName(form.getName());
        p.setDescription(form.getDescription());
        p.setPrice(Math.round(form.getPrice() * 100));

        // recherche la catégorie en base
        Category c = categories.findById(form.getCategory()).orElseThrow(() -> new RuntimeException("Not found"));
        p.setCategory(c);

        // s'assure que tag n'est pas null
        if (p.getTags() == null) {
            p.setTags(new ArrayList<>());
        }

        // ajoute les tags à la liste
        p.getTags().clear(); // supprime les tags existant, pour ajouter ceux sélectionnés
        for (Long tagId: form.getTags()) {
            Tag t = tags.findById(tagId).orElseThrow(() -> new RuntimeException("Not found"));
            p.getTags().add(t);
        }

        // sauvegarde en base
        products.save(p);

        // sauvegarde l'image
        if (picture != null && picture.isEmpty() == false) {
            // délègue les manipulations au service
            // le service nécessite que "p" possède un id, il doit donc avoir été sauvegardé avant !
            pictureService.save(p, picture);

            // et sauvegarde à nouveau le produit qui a été modifié
            products.save(p);
        }

        return "redirect:/product/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        Product p = products.findById(id).orElseThrow(() -> new RuntimeException("Not found"));
        if (p.getPictureOriginalFilename() != null) {
            pictureService.delete(p);
        }

        products.delete(p);
        return "redirect:/product/list";
    }

    @GetMapping("/picture/{id}")
    @ResponseBody
    public ResponseEntity<Resource> getPicture(@PathVariable("id") Long productId) {
        Product p = products.findById(productId).orElseThrow(() -> new RuntimeException("Not found"));

        Resource file = pictureService.loadAsResource(p);
        if(file == null) {
            throw new RuntimeException("Error lors du téléchargement de l'image");
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + p.getPictureOriginalFilename() + "\"")
                .body(file);
    }

    @GetMapping("/picture/delete/{id}")
    public String removePicture(@PathVariable("id") Long productId) {
        Product p = products.findById(productId).orElseThrow(() -> new RuntimeException("Not found"));

        pictureService.delete(p);
        products.save(p);

        return "redirect:/product/edit/" + p.getId();
    }
}
