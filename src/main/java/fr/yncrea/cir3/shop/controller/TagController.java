package fr.yncrea.cir3.shop.controller;

import fr.yncrea.cir3.shop.domain.Category;
import fr.yncrea.cir3.shop.domain.Tag;
import fr.yncrea.cir3.shop.form.CategoryForm;
import fr.yncrea.cir3.shop.form.TagForm;
import fr.yncrea.cir3.shop.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/tag")
@Secured("ROLE_ADMIN")
public class TagController {
    @Autowired
    private TagRepository tags;

    @GetMapping({"/", "/list"})
    public String list(Model model) {
        model.addAttribute("tags", tags.findAll());
        return "tag/list";
    }

    @GetMapping({"/add", "edit/{id}"})
    public String add(@PathVariable(required = false) Long id, Model model) {
        TagForm form = new TagForm();
        model.addAttribute("tag", form);

        if (id != null) {
            Tag t = tags.findById(id).orElseThrow(() -> new RuntimeException("Not found"));

            form.setId(t.getId());
            form.setName(t.getName());
        }

        return "tag/add";
    }

    @PostMapping("/add")
    public String addAction(@Valid @ModelAttribute("tag") TagForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("tag", form);
            return "tag/add";
        }

        Tag t = new Tag();

        if (form.getId() != null) {
            t = tags.findById(form.getId()).orElseThrow(() -> new RuntimeException("Not found"));
        }

        t.setName(form.getName());
        tags.save(t);

        return "redirect:/tag/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        tags.deleteById(id);
        return "redirect:/tag/list";
    }
}
