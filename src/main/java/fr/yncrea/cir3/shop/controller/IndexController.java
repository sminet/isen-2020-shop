package fr.yncrea.cir3.shop.controller;

import fr.yncrea.cir3.shop.form.CartProductForm;
import fr.yncrea.cir3.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @Autowired
    private ProductRepository products;

    @GetMapping({"", "/"})
    public String index(Model model, @PageableDefault(page = 0, size = 10) Pageable pageable) {
        // Formulaire d'ajout de produit
        CartProductForm form = new CartProductForm();
        form.setQuantity(1L);

        model.addAttribute("products", products.findAll(pageable));
        model.addAttribute("form", form);

        return "index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
