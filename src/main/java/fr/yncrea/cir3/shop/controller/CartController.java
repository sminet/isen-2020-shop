package fr.yncrea.cir3.shop.controller;

import fr.yncrea.cir3.shop.domain.Product;
import fr.yncrea.cir3.shop.form.CartProductForm;
import fr.yncrea.cir3.shop.form.ProductForm;
import fr.yncrea.cir3.shop.repository.ProductRepository;
import fr.yncrea.cir3.shop.service.CartProductDto;
import fr.yncrea.cir3.shop.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cart;

    @Autowired
    private ProductRepository products;

    @PostMapping("/add")
    public String add(HttpServletRequest request, @Valid @ModelAttribute CartProductForm form, BindingResult result, Model model) {
        cart.addProduct(form.getProductId(), form.getQuantity());

        // redirige vers la page précédente
        String referer = request.getHeader("Referer");
        if (referer == null) {
            referer = "/";
        }

        return "redirect:"+ referer;
    }

    @GetMapping({"", "/"})
    public String show(Model model) {
        Map<Product, Long> data = new LinkedHashMap<>();

        for (CartProductDto product: cart.getProducts()) {
            // récupération du produit en base
            Product p = products.findById(product.getProductId()).orElseThrow();

            // ajout dans la map
            data.put(p, product.getQuantity());
        }

        model.addAttribute("cart", data);

        return "cart/show";
    }
}
