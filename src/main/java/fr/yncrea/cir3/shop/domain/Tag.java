package fr.yncrea.cir3.shop.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Tag {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;
}
