package fr.yncrea.cir3.shop.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Product {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 255)
    private String description;

    @Column
    private Long price;

    @ManyToOne
    private Category category;

    @ManyToMany
    private List<Tag> tags;

    private String pictureOriginalFilename;
}
